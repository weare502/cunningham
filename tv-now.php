<?php
/**
 * Template Name: TV Now
  *
  * @package  WordPress
  * @subpackage  Timber
  */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;

$tv_now_options = get_field('tv_now_options', 'option' );

$context['tv_now_options'] = $tv_now_options;

Timber::render( array( 'tv-now.twig' ), $context );