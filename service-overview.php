<?php
/**
 * Template Name: Service Overview
 * 
 * @package  WordPress
 * @subpackage  Timber
 */

$post = new TimberPost();

add_filter('body_class', function( $classes ) use ($post){
	$classes[] = $post->slug;
	return $classes;
} );

$context = Timber::get_context();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;

if ( $post->slug == 'business' ){
	$context['business'] = true;
}

Timber::render( array( 'service-overview.twig' ), $context );