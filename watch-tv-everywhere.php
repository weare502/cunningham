<?php
/**
 * Template Name: Watch TV Everywhere
  *
  * @package  WordPress
  * @subpackage  Timber
  */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;

$context['channels'] = Timber::get_posts( new WP_Query( array( 
	'post_type' => 'tveverywhere_channel',
	'posts_per_page' => -1,
	'orderby' => 'post_title',
	'order' => 'ASC'
) ) );

Timber::render( array( 'watch-tv-everywhere.twig' ), $context );