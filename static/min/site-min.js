/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
String.prototype.startsWith||(String.prototype.startsWith=function(t,e){return e=e||0,this.substr(e,t.length)===t})
/**
 * FitVids JS
 */,function(n){"use strict";n.fn.fitVids=function(t){var i={customSelector:null,ignore:null};if(!document.getElementById("fit-vids-style")){
// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
var e=document.head||document.getElementsByTagName("head")[0],a=".fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}",o=document.createElement("div");o.innerHTML='<p>x</p><style id="fit-vids-style">'+a+"</style>",e.appendChild(o.childNodes[1])}return t&&n.extend(i,t),this.each(function(){var t=['iframe[src*="player.vimeo.com"]','iframe[src*="youtube.com"]','iframe[src*="youtube-nocookie.com"]','iframe[src*="kickstarter.com"][src*="video.html"]',"object","embed"];i.customSelector&&t.push(i.customSelector);var r=".fitvidsignore";i.ignore&&(r=r+", "+i.ignore);var e=n(this).find(t.join(","));// Disable FitVids on this video.
(// SwfObj conflict patch
e=(e=e.not("object object")).not(r)).each(function(){var t=n(this);if(!(0<t.parents(r).length||"embed"===this.tagName.toLowerCase()&&t.parent("object").length||t.parent(".fluid-width-video-wrapper").length)){t.css("height")||t.css("width")||!isNaN(t.attr("height"))&&!isNaN(t.attr("width"))||(t.attr("height",9),t.attr("width",16));var e,i,a=("object"===this.tagName.toLowerCase()||t.attr("height")&&!isNaN(parseInt(t.attr("height"),10))?parseInt(t.attr("height"),10):t.height())/(isNaN(parseInt(t.attr("width"),10))?t.width():parseInt(t.attr("width"),10));if(!t.attr("name")){var o="fitvid"+n.fn.fitVids._count;t.attr("name",o),n.fn.fitVids._count++}t.wrap('<div class="fluid-width-video-wrapper"></div>').parent(".fluid-width-video-wrapper").css("padding-top",100*a+"%"),t.removeAttr("height").removeAttr("width")}})})},
// Internal counter for unique video names.
n.fn.fitVids._count=0}(window.jQuery||window.Zepto),
/* jshint ignore:end */
jQuery(document).ready(function(a){function t(t){t.preventDefault(),a(".tab-content").hide(),a(".tab-switch").removeClass("active-tab");var e=a(this);e.addClass("active-tab");var i=e.attr("data-tab-target");a("[data-tab="+i+"]").show()}var e=a("#menu-toggle"),i=a("body"),o=a("#header");i.fitVids(),i.on("click","#menu-toggle",function(){o.toggleClass("nav-open"),e.toggleClass("fa-bars"),e.toggleClass("fa-close")}),a(".open-popup-link").magnificPopup({type:"inline",removalDelay:300,midClick:!0}),a(".home-slider").slick({arrows:!1,dots:!0,autoplay:!0,autoplaySpeed:5e3}),a(".tab-switch").on("click",t),a("#nav-main li").last().find("a").on("click",function(t){t.preventDefault(),a("#bill-pay-form").submit()}),a('a[href*="/pay-bill/"]').click(function(t){t.preventDefault(),a("#bill-pay-form").submit()}),a(".tab-content").hide(),a(".tab-content").first().show(),a(".tabs .tab-switch").removeClass("active-tab"),a(".tabs .tab-switch").first().addClass("active-tab"),window.showServiceContactFormModal=function(t){console.log(t);var e=a("#service-contact-form").html();jQuery("#results-popup").html(""),jQuery.magnificPopup.open({items:{src:"#results-popup",// can be a HTML string, jQuery object, or CSS selector
type:"inline"},removalDelay:300}),
// Magnific Popup duplicates our element so we need to find it again.
jQuery("#results-popup").append(e),a("#input_1_5").val(t)}});// End Document Ready