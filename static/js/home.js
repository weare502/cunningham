/* global Vue, _ */
var vm = new Vue({
	el: '#vue-wrapper',
	data: {
		cities: window.cunninghamCities,
		currentCity: '',
		zip: '',
		currentCityTypes: [],
		chosenType: '',
		filteredCities: [],
		resultsPopup: jQuery('#results-popup'),
		intownUrl: '',
		ruralUrl: '',
	},
	methods: {
		setChosenType: function( type ){
			this.chosenType = type;
		},
		getCityFromZip: function(){
			if ( this.zip === '' ){
				this.showAllCitiesPopup();
				return;
			}
			//make a request to the google geocode api
			jQuery.getJSON('//maps.googleapis.com/maps/api/geocode/json?address='+this.zip).success(function(response){
				
				//find the city and state
				var address_components = response.results[0].address_components;
				var cityName, state, country;
				
				jQuery.each(address_components, function(index, component){
					var types = component.types;
					jQuery.each(types, function(index, type){
						if(type === 'locality') {
							cityName = component.long_name;
						}
						if(type === 'administrative_area_level_1') {
							state = component.short_name;
						}
						if(type === 'country') {
							country = component.short_name;
						}
					});
				});

				vm.$data.currentCity = cityName;
			} );
		},
		showPopup: function( content ){

			jQuery('#results-popup').html('');
			
			jQuery.magnificPopup.open({
				items: {
					src: '#results-popup', // can be a HTML string, jQuery object, or CSS selector
					type: 'inline'
				},
				removalDelay: 300
			});

			// Magnific Popup duplicates our element so we need to find it again.
			jQuery('#results-popup').append( content );

		},
		showChooseTypePopup: function(){
			var html = jQuery('#choose-service-type-vue').html();
			this.showPopup( html );
		},
		showChooseLocationPopup: function(){

			this.intownUrl = _.findWhere( this.filteredCities, { name: this.currentCity } ).url;
			this.ruralUrl = _.findWhere( this.filteredCities, { name: this.currentCity + " (Rural)" } ).url;

			// wait for DOM to update then fire our new popup
			this.$nextTick( function(){
				var html = jQuery('#choose-city-location-vue').html();
				this.showPopup( html );
			});

		},
		goToCity: function( city ){
			window.location = city.url;
		},
		resetVars: function(){
			// reset vars if zip is changed
			this.currentCity = '';
			this.currentCityTypes = [];
			this.chosenType = '';
			this.filteredCities = [];
			this.intownUrl = '';
			this.ruralUrl = '';
		},
		showAllCitiesPopup: function(){

			this.resetVars();

			var html = jQuery('#show-all-cities-vue').html();

			this.showPopup( html );
		}
	},
	watch: {
		'zip': function(){
			this.resetVars();
		},
		'currentCity': function( val ){

			// value was reset so don't do anything
			if ( val === '' ) {
				return; }

			var cityName = val;
			
			this.filteredCities = _.filter( this.cities, function( city ){
				return city.name.startsWith( cityName );
			} );

			if ( this.filteredCities.length === 0 ){
				this.showAllCitiesPopup();
				return;
			}
			
			this.currentCityTypes = _.union( _.pluck( this.filteredCities, 'type' ) );

		},
		'currentCityTypes': function( val ){
			
			// value was reset so don't do anything
			if ( val.length === 0 ) { return; }

			if ( val.length > 1 ){
				this.showChooseTypePopup();
			} else {
				this.chosenType = val[0];
			}
		},
		'chosenType': function( val ){

			// value was reset so don't do anything
			if ( val === '' ) { return; }

			var currentCity = this.currentCity;

			this.filteredCities = _.filter( this.cities, function( city ){
				return city.name.startsWith( currentCity ) && city.type === val;
			} );

			if ( this.filteredCities.length > 1 ){
				this.showChooseLocationPopup();
			} else {
				this.goToCity( this.filteredCities[0] );
			}
		}
	}
});