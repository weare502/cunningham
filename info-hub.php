<?php
/**
 * Template Name: Info Hub
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$post->thumbnail = $post->get_thumbnail();
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;

$context['featured_post'] = new TimberPost( ( new WP_Query(array(
	'post_type' => 'post',
	'posts_per_page' => 1,
	'meta_query' => array(
		array(
			'key'     => 'featured',
			'value'   => true,
			'compare' => '=',
		),
	),
) ) )->posts[0] );

$context['community_posts'] = Timber::get_posts( array( 
	'post_type' => 'post',
	'posts_per_page' => 5,
	'category__in' => 19
) );

$context['service_posts'] = Timber::get_posts( array( 
	'post_type' => 'post',
	'posts_per_page' => 5,
	'category__in' => 20
) );

$context['company_posts'] = Timber::get_posts( array( 
	'post_type' => 'post',
	'posts_per_page' => 5,
	'category__in' => 21
) );

$context['blog_posts'] = Timber::get_posts( array( 
	'post_type' => 'post',
	'posts_per_page' => 5,
	'category__not_in' => array(19, 20, 21)
) );

Timber::render( array( 'info-hub.twig' ), $context );