<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;

/**
 * Bundle Options
 */
$bundles = get_field( 'bundles', 'option' );
$chosen_bundles = get_field( 'bundles_repeater' );

foreach( $bundles as $key => $value ){
	$id = $value['bundle_id'];
	$keep = false;
	
	if ( $chosen_bundles ) {
		foreach ( $chosen_bundles as $bundle ){
			// strict type check or $bundle['coming_soon'] causes false positive
			if ( in_array( $id, $bundle, true ) ){
				$keep = true;
				$value['coming_soon'] = $bundle['coming_soon'];
				break;
			}
		}
	}
	
	if ( $keep ){
		$bundles[$value['bundle_id']] = $value;
	}

	unset( $bundles[$key] );
}
if ( $chosen_bundles ){
	foreach ( $chosen_bundles as $key => $value ){
		$bundles[$key] = $bundles[$value['bundle_id']];
		unset( $bundles[$value['bundle_id']]);
	}
}



/**
 * Phone Options
 */
$phone_options = get_field('phone_options', 'options' );
$chosen_phone_options = get_field('phone_options', $post->ID );

foreach ( $phone_options as $key => $value ){
	$id = $value['option_id'];
	$keep = false;
	
	if ( $chosen_phone_options ) {
		foreach ( $chosen_phone_options as $option ){
			if ( in_array( $id, $option ) ){
				$keep = true;
				break;
			}
		}
	}
	
	if ( $keep ){
		$phone_options[$value['option_id']] = $value;
	}

	unset( $phone_options[$key] );
}

if ( $chosen_phone_options ){
	foreach ( $chosen_phone_options as $key => $value ){
		$phone_options[$key] = $phone_options[$value['option_id']];
		unset( $phone_options[$value['option_id']]);
	}
}

/**
 * Cable Options
 */
$cable_options = get_field('cable_options', 'option' );
$chosen_cable_options = get_field( 'cable_options', $post->ID );

foreach ( $cable_options as $key => $value ){
	$id = $value['option_id'];
	$keep = false;
	
	if ( $chosen_cable_options ) {
		foreach ( $chosen_cable_options as $option ){
			if ( in_array( $id, $option ) ){
				$keep = true;
				break;
			}
		}
	}
	
	if ( $keep ){
		$cable_options[$value['option_id']] = $value;
	}

	unset( $cable_options[$key] );
}

if ( $chosen_cable_options ){
	foreach ( $chosen_cable_options as $key => $value ){
		$cable_options[$key] = $cable_options[$value['option_id']];
		unset( $cable_options[$value['option_id']]);
	}
}

/**
 * Internet Options
 */
$internet_options = get_field('internet_options', 'option' );
$chosen_internet_options = get_field( 'internet_options', $post->ID );

foreach ( $internet_options as $key => $value ){
	$id = $value['option_id'];
	$keep = false;
	
	if ( $chosen_internet_options ) {
		foreach ( $chosen_internet_options as $option ){
			if ( in_array( $id, $option ) ){
				$keep = true;
				break;
			}
		}
	}
	
	if ( $keep ){
		$internet_options[$value['option_id']] = $value;
	}

	unset( $internet_options[$key] );
}

if ( $chosen_internet_options ){
	foreach ( $chosen_internet_options as $key => $value ){
		$internet_options[$key] = $internet_options[$value['option_id']];
		unset( $internet_options[$value['option_id']]);
	}
}

/**
 * Internet Options
 */
$tv_now_options = get_field('tv_now_options', 'option' );
$chosen_tv_now_options = get_field( 'tv_now_options', $post->ID );

foreach ( $tv_now_options as $key => $value ){
	$id = $value['option_id'];
	$keep = false;
	
	if ( $chosen_tv_now_options ) {
		foreach ( $chosen_tv_now_options as $option ){
			if ( in_array( $id, $option ) ){
				$keep = true;
				break;
			}
		}
	}
	
	if ( $keep ){
		$tv_now_options[$value['option_id']] = $value;
	}

	unset( $tv_now_options[$key] );
}

if ( $chosen_tv_now_options ){
	foreach ( $chosen_tv_now_options as $key => $value ){
		$tv_now_options[$key] = $tv_now_options[$value['option_id']];
		unset( $tv_now_options[$value['option_id']]);
	}
}

$context['bundles'] = $bundles;
$context['phone_options'] = $phone_options;
$context['cable_options'] = $cable_options;
$context['internet_options'] = $internet_options;
$context['tv_now_options'] = $tv_now_options;

Timber::render( array( 'single-city.twig' ), $context );