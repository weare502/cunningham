<?php
/**
 * Template Name: About Us
 * 
 * @package  WordPress
 * @subpackage  Timber
 */

$post = new TimberPost();

add_filter('body_class', function( $classes ) use ($post){
	$classes[] = $post->slug;
	return $classes;
} );

$context = Timber::get_context();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;
$context['owner_photo'] = new TimberImage( $post->owner_photo );
$context['glen_elder_photo'] = new TimberImage( $post->glen_elder_photo );
$context['belleville_photo'] = new TimberImage( $post->belleville_photo );
$context['beloit_photo'] = new TimberImage( $post->beloit_photo );
$context['concordia_photo'] = new TimberImage( $post->concordia_photo );


Timber::render( array( 'about-us.twig' ), $context );