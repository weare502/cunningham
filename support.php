<?php
/**
 * Template Name: Support Template
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$post->thumbnail = $post->get_thumbnail();
$context['header_hero_image'] = ! empty( $post->thumbnail ) ? $post->thumbnail->src : false;
Timber::render( 'support.twig', $context );