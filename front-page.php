<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();

$context['internet_services'] = get_field( 'internet_services' );
$context['cable_services'] = get_field( 'cable_services' );
$context['telephone_services'] = get_field( 'telephone_services' );
$context['business_services'] = get_field( 'business_services' );

$context['slides'] = get_field( 'home_slider' );

foreach ( $context['slides'] as &$slide ) {
	$slide['image'] = new TimberImage( $slide['image'] );
}

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );
