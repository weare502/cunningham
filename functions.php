<?php
/**
 * The main theme functions file.
 *
 * @package Timber
 * @since  1.0
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );
	return;
}

Timber::$dirname = array( 'templates', 'views' );

class CunninghamSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 100 );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );

		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'template_include', array( $this, 'include_city_template' ) );

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_uri() );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['cities'] = $this->get_unique_cities();
		$context['cachebust'] = time();
		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation - (top of site)' );
		register_nav_menu( 'secondary', 'Secondary Navigation - (top of site, small, above main)' );
		register_nav_menu( 'footer_service_links', 'Footer Existing Customer Links' );
		register_nav_menu( 'footer_account_links', 'Footer Info Links' );
	}

	function enqueue_scripts() {

		wp_enqueue_script( 'cunningham-slick', get_template_directory_uri() . '/static/js/min/slick.min.js', array( 'jquery' ), '20160802' );

		wp_enqueue_script( 'cunningham-theme', get_template_directory_uri() . '/static/site.js', array( 'jquery', 'underscore', 'magnific-popup', 'cunningham-slick' ), '20160806' );

		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );

		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );

		$this->localize_cities();

	}

	function include_city_template( $template ) {
		global $post;
		$types = array( 'residential_city', 'business_city' );

		if ( isset( $post->post_type ) && in_array( $post->post_type, $types ) ) {
			$new_template = locate_template( array( 'single-city.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
		}

		return $template;
	}

	function admin_head_css() {
		?>
		<style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style>
		<?php
	}

	function get_unique_cities() {
		$residential_cities = new WP_Query( array( 'post_type' => 'residential_city', 'posts_per_page' => -1 ) );
		$business_cities = new WP_Query( array( 'post_type' => 'business_city', 'posts_per_page' => -1 ) );

		$res_city_names = array();
		$bus_city_names = array();

		if ( $residential_cities->have_posts() ) {
			foreach ( $residential_cities->posts as $city ) {
				$res_city_names[] = $city->post_title;
			}
		}

		if ( $business_cities->have_posts() ) {
			foreach ( $business_cities->posts as $city ) {
				$bus_city_names[] = $city->post_title;
			}
		}

		$cities = array_unique( array_merge( $res_city_names, $bus_city_names ) );

		foreach ( $cities as $key => $value ) {
			if ( strpos( $value, '(Rural)' ) !== false ) {
				unset( $cities[ $key ] );
			}
		}

		sort( $cities );

		return $cities;
	}

	function localize_cities() {
		$residential_cities = new WP_Query( array( 'post_type' => 'residential_city', 'posts_per_page' => -1 ) );
		$business_cities = new WP_Query( array( 'post_type' => 'business_city', 'posts_per_page' => -1 ) );
		$args = array();

		if ( $residential_cities->have_posts() ) {
			foreach ( $residential_cities->posts as $city ) {
				$args[] = array( 'name' => $city->post_title, 'url' => get_permalink( $city->ID ), 'type' => 'residential' );
			}
		}

		if ( $business_cities->have_posts() ) {
			foreach ( $business_cities->posts as $city ) {
				$args[] = array( 'name' => $city->post_title, 'url' => get_permalink( $city->ID ), 'type' => 'business' );
			}
		}

		wp_localize_script( 'cunningham-theme', 'cunninghamCities', $args );
	}

	/**
	 * Add the style formats dropdown.
	 *
	 * @param  array $buttons Strings of the different buttons in TinyMCE.
	 * @return array
	 */
	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings.
	function tiny_mce_insert_formats( $init_array ) {
		// Define the style_formats array.
		$style_formats = array(
			// Each array child is a format with it's own settings.
			array(
				'title' => 'Button',
				'selector' => 'a',
				'classes' => 'button',
				// Font awesome must be available in the admin area to see the icon.
				'icon'	   => ' fa fa-hand-pointer-o',
			),
			array(
				'title' => 'Emphasized Heading',
				'selector' => 'h1, h2, h3, h4, h5, h6',
				'classes' => 'emphasized-heading',
				// Font awesome must be available in the admin area to see the icon.
				'icon'	   => ' fa fa-exclamation',
			),
		);
		// Insert the array, JSON ENCODED, into 'style_formats'.
		$init_array['style_formats'] = json_encode( $style_formats );

		return $init_array;

	}
}

new CunninghamSite();

function cunningham_primary_nav() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
	) );
}
function cunningham_secondary_nav() {
	wp_nav_menu( array(
		'theme_location' => 'secondary',
		'container' => '',
	) );
}

function cunningham_footer_service_links_nav() {
	wp_nav_menu( array(
		'theme_location' => 'footer_service_links',
		'container' => '',
	) );
}
function cunningham_footer_account_links_nav() {
	wp_nav_menu( array(
		'theme_location' => 'footer_account_links',
		'container' => '',
	) );
}

function cunningham_render_service_contact_form() {
	if ( function_exists( 'gravity_form' ) ) {
		gravity_form( 1, false, false, false, '', true );
	} else {
		echo "<p>The form doesn't seem to be working.</p> <p>Pleasse give us a call if you see this message.</p> <p>We apologize for any inconvenience.</p> <p><a class='button' href='tel:18002878495'>1 (800) 287-8495</a>";
	}
}
