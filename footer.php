<?php
/**
 * Third party plugins that hijack the theme will call wp_footer() to get the footer template.
 * We use this to end our output buffer (started in header.php) and render into the view/page-plugin.twig template.
 *
 * If you're not using a plugin that requries this behavior (ones that do include Events Calendar Pro and
 * WooCommerce) you can delete this file and header.php
 *
 * @package Timber
 */

$timber_context = $GLOBALS['timberContext'];
if ( ! isset( $timber_context ) ) {
	throw new \Exception( 'Timber context not set in footer.' );
}
$timber_context['content'] = ob_get_contents();
ob_end_clean();

wp_reset_postdata();

if (
	( is_archive() && is_post_type_archive( 'kbe_knowledgebase' ) )
		||
	( is_archive() && is_tax( 'kbe_taxonomy' ) )
		||
	( is_single() && 'kbe_knowledgebase' == $post->post_type )
) {
	$timber_context['post'] = new TimberPost( get_post( 203 ) );
	$timber_context['post']->thumbnail = $timber_context['post']->get_thumbnail();
	$timber_context['header_hero_image'] = ! empty( $timber_context['post']->thumbnail ) ? $timber_context['post']->thumbnail->src : false;
	$timber_context['post']->title = 'Support';
}

$templates = array( 'page-plugin.twig' );
Timber::render( $templates, $timber_context );
